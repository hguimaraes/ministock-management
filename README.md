## ministock-management

> Prova de conceitos de uma Web API em C# para realizar operações (Ordem) em um determinado conjunto de Ativos 

### Como utilizar

A aplicação foi desenvolvida em C# no Visual Studio 2017, utilizando o framework .NET 4.5 e Microsoft SQL Server 2016 como banco de dados. 

Na pasta "assets" encontra-se um script SQL para a criação das tabelas na base de dados e uma carga mínima de exemplo com dois ativos. No ambiente de desenvolvimento o nome da base de dados era "ministock" e a autenticação era realizada através da autenticação do Windows.

Para executar o programa basta fazer um clone deste repositório, abrir a solução no Visual Studio e clicar no botão de Run.

### Implementação

O projeto consiste em uma Web API do C#, que utiliza os principais conceitos de Restful API. As requisições e respostas HTTP realizadas para a API utilizam o formato JSON. Temos duas rotas principais: a */api/Ativo* e a */api/Ordem*

#### Utilização

* Ativo

| Método | Rota | Descrição |
|--------|------|-----------|
| GET | api/Ativo   | Método para listar todos os ativos cadastrados no BD |
| GET | api/Ativo/{id} | Método para retornar o ativo com id_ativo = {id} no BD |
| PUT | api/Ativo/{id} | Método para atualizar um determinado ativo no BD |
| POST | api/Ativo | Método para cadastrar um novo ativo |

* Ordem

| Método | Rota | Descrição |
|--------|------|-----------|
| GET | api/Ordem   | Método para listar todos as Ordens registradas no BD |
| GET | api/Ordem/{id} | Método para retornar uma ordem específica no BD |
| POST | api/Ordem | Método para registrar uma nova Ordem no BD |
| GET | api/Ordem/data/{data} | Método para listar Ordens de uma data específica |
| GET | api/Ordem/posicao/{ativoId} | Cálculo da posição de um determinado ativo |

### Regras

Para inserir novos dados, o usuário deve enviar as informações (Do Ativo ou da Ordem) no Body do Request, usando codificação x-www-form-urlencoded. As chaves de cada valor a ser inserido devem possuir o mesmo nome dos atributos que foram modelados, mas o id não precisa ser preenchido:

Para o Ativo:

* *descricao*: Nome do Ativo
* *lote_minimo*: Quantiddade mínima que pode ser comprada/vendida. Batches de ativos devem ser múltiplos desse número

Para a Ordem:

* *fk_id_ativo*: ID do ativo
* *quantidade*: Quantidade a ser comprada/vendida
* *preco*: Número real
* *classe_negociacao*: C (Compra) ou V (Venda)
* *data*: Data da negociação no formato YYYY-MM-DD

O não cumprimento de alguma das regras (Por exemplo, inserir uma letra diferente em *classe_negociacao*, inserir uma *quantidade* negativa e outros, *data* fora do padrão definido e afins) retornará um código de erro e uma mensagem para o usuário.

Exemplo de utilização do Post de uma Ordem no Postman:

![](assets/imgs/PostOrdem.JPG)

Exemplo de utilização do Post de uma Ordem com erro no valor da Classe de negociação no Postman:

![](assets/imgs/PostOrdemErrorClasse.JPG)

Exemplo de utilização do Post de uma Ordem com erro no valor da Quantidade (Não é múltiplo de *lote_minimo*) no Postman:

![](assets/imgs/PostOrdemErrorQuantidade.JPG)

Exemplo de utilização do GET de uma Ordem no Postman:

![](assets/imgs/GetPosicao.JPG)