﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ministock_management.Models;

namespace ministock_management.Controllers
{
    public class AtivoController : ApiController
    {
        private ministockEntities db = new ministockEntities();

        // GET: api/Ativo
        public IQueryable<Ativo> GetAtivo()
        {
            return db.Ativo;
        }

        // GET: api/Ativo/5
        [ResponseType(typeof(Ativo))]
        public IHttpActionResult GetAtivo(int id)
        {
            Ativo ativo = db.Ativo.Find(id);
            if (ativo == null)
            {
                return NotFound();
            }

            return Ok(ativo);
        }

        // PUT: api/Ativo/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAtivo(int id, [FromBody] Ativo ativo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ativo.id_ativo)
            {
                return BadRequest();
            }

            db.Entry(ativo).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AtivoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.Accepted);
        }

        // POST: api/Ativo
        [ResponseType(typeof(Ativo))]
        public IHttpActionResult PostAtivo([FromBody] Ativo ativo)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            db.Ativo.Add(ativo);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = ativo.id_ativo }, ativo);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AtivoExists(int id)
        {
            return db.Ativo.Count(e => e.id_ativo == id) > 0;
        }
    }
}