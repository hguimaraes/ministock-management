﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ministock_management.Models;
using ministock_management.Services;

namespace ministock_management.Controllers
{
    [RoutePrefix("api/Ordem")]
    public class OrdemController : ApiController
    {
        private ministockEntities db = new ministockEntities();
        private OrdemService ordemService = new OrdemService();

        // GET: api/Ordem
        [Route("")]
        public IQueryable<Ordem> GetOrdem()
        {
            return db.Ordem;
        }

        // GET: api/Ordem/5
        [Route("{id:int}")]
        [ResponseType(typeof(Ordem))]
        public IHttpActionResult GetOrdem(int id)
        {
            Ordem ordem = db.Ordem.Find(id);
            if (ordem == null)
            {
                return NotFound();
            }

            return Ok(ordem);
        }

        // POST: api/Ordem
        [HttpPost]
        [Route("", Name = "PostRoute")]
        [ResponseType(typeof(Ordem))]
        public IHttpActionResult PostOrdem([FromBody] Ordem ordem)
        {
            Ativo ativo = db.Ativo.Find(ordem.fk_id_ativo);

            if (!ModelState.IsValid || ativo == null) {
                return BadRequest(ModelState);
            }

            // Set Ativo to Ordem object
            ordem.Ativo = ativo;

            if (!ordemService.ValidateOrdemPOST(ordem)) {
                string msg = "Error in one of the post fields: quantidade or classe_negociacao";
                return BadRequest(msg);
            }

            db.Ordem.Add(ordem);
            db.SaveChanges();
            
            return CreatedAtRoute("PostRoute", new { id = ordem.id_ordem }, ordem);
        }
        
        // GET: api/Ordem/2017-09-15
        [Route("data/{data}")]
        [ResponseType(typeof(IQueryable<Ordem>))]
        public IHttpActionResult GetOrdemByData(System.DateTime data)
        {
            IQueryable<Ordem> ordem = db.Ordem.Where(i => i.data == data);
            if (!ordem.Any()) {
                return NotFound();
            }

            return Ok(ordem);
        }

        // GET: api/Ordem/Posicao/1 (Posicao de um Ativo)
        [Route("posicao/{ativoId}")]
        [ResponseType(typeof(Posicao))]
        public IHttpActionResult GetOrdemPosicao(int ativoId) {
            IQueryable<Ordem> ordem = db.Ordem.Where(i => i.fk_id_ativo == ativoId);
            if (!ordem.Any()) {
                return NotFound();
            }

            Posicao result = ordemService.CalculatePosition(ativoId, ordem);
            return Ok(result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrdemExists(int id)
        {
            return db.Ordem.Count(e => e.id_ordem == id) > 0;
        }
    }
}