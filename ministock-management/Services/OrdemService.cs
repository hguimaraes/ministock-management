﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ministock_management.Models;

namespace ministock_management.Services
{
    public class OrdemService {
        public OrdemService() {}

        public bool ValidateOrdemPOST(Ordem ordem) {
            bool state = true;
            
            // Verificar o tipo de operação (Somente C e V são aceitas)
            if (ordem.classe_negociacao != "C" && ordem.classe_negociacao != "V"){
                state = false;
            }

            // Verificar se a quantidade é múltipla da quantidade mínima
            if (ordem.quantidade <= 0 || ordem.quantidade % ordem.Ativo.lote_minimo != 0){
                state = false;
            }
            return state;
        }

        public Posicao CalculatePosition(int id, IQueryable<Ordem> ordem) {
            double position = 0;
            foreach (var ord in ordem) {
                if (ord.classe_negociacao == "C") {
                    position += ord.quantidade;
                } else {
                    position += -ord.quantidade;
                }
            }

            // Resultado da Operação de Posição
            Posicao result = new Posicao {
                Id = id,
                Pos = position
            };

            return result;
        }
    }
}