


/****************************************************************
Criando tabela de ativos
*****************************************************************/

CREATE TABLE [dbo].[Ativo](
	[id_ativo] [int] IDENTITY(1,1) NOT NULL,
	[descricao] [varchar](30) NULL,
	[lote_minimo] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_ativo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



GO


/****************************************************************
Criando tabela de Ordens
*****************************************************************/

CREATE TABLE [dbo].[Ordem](
	[id_ordem] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_ativo] [int] NOT NULL,
	[quantidade] [float] NOT NULL,
	[preco] [float] NOT NULL,
	[data] [date] NOT NULL,
	[classe_negociacao] [char](1) NOT NULL,
CONSTRAINT [PK__Ordem__DD5B8F32C66448BB] PRIMARY KEY CLUSTERED 
(
	[id_ordem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Ordem]  WITH CHECK ADD  CONSTRAINT [FK_Ordem_Ativo] FOREIGN KEY([fk_id_ativo])
REFERENCES [dbo].[Ativo] ([id_ativo])
GO

ALTER TABLE [dbo].[Ordem] CHECK CONSTRAINT [FK_Ordem_Ativo]
GO


/****************************************************************
Carga minima de ativos
*****************************************************************/

INSERT INTO dbo.Ativo
        ( descricao, lote_minimo )
VALUES  ( 'Petr4', -- descricao - varchar(30)
          100  -- lote_minimo - int
          )

INSERT INTO dbo.Ativo
        ( descricao, lote_minimo )
VALUES  ( 'INDV17', -- descricao - varchar(30)
          1  -- lote_minimo - int
          )

